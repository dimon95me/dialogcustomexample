package examplesoureit.dialogcustomexample;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button action;

    Dialog myDialog;

    Button hello, close;

    TextView showText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        action = findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCustomAlertDialog();
            }
        });
    }

    public void myCustomAlertDialog() {
        myDialog = new Dialog(MainActivity.this);
        myDialog.setContentView(R.layout.custom_dialog);
        myDialog.setTitle("My custon dialog");

        hello = (Button) myDialog.findViewById(R.id.hello);
        close = (Button) myDialog.findViewById(R.id.close);

        hello.setEnabled(true);
        close.setEnabled(true);

        showText = myDialog.findViewById(R.id.show_text);

        hello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showText.setText("Hi, I'm your custon dialog!");
                Toast.makeText(getApplicationContext(), "Hi, I'm your custon dialog!", Toast.LENGTH_SHORT).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.cancel();
            }
        });
        myDialog.show();
    }
}
